private void InternalMergeSort(long[] inputArray, int left, int right)
{
    int mid = 0;
    if (left < right)
    {
        mid = (left + right) / 2;
        InternalMergeSort(inputArray, left, mid);
        InternalMergeSort(inputArray,(mid + 1), right);
        MergeSortedArray(inputArray, left, mid, right);
    }
}